
### First Try 
Nexus9000v �� VxLAN+EVPN (anycast gateway ��)
https://kakkotetsu.hatenablog.com/entry/2017/09/12/000405







Cisco Nexus 9000v Guide, Release 10.1(x)
https://www.cisco.com/c/en/us/td/docs/dcn/nx-os/nexus9000/101x/configuration/nx-osv/cisco-nexus-9000v-guide-101x/m-troubleshooting-cisco-nexus-9000v.html

How to prevent VM from dropping into "loader >" prompt
As soon as you set up your Cisco Nexus 9000v (following set up of POAP interface), you need to configure the boot image in your system to avoid 
dropping to the "loader >" prompt after reload/shut down.

Example:

config t
  boot nxos n9000-dk9.7.0.3.I2.0.454.bin
copy running starting