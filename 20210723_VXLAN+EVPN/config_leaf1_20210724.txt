show run

!Command: show running-config
!Running configuration last done at: Fri Jul 23 22:33:09 2021
!Time: Sat Jul 24 02:50:10 2021

version 9.3(7) Bios:version  
feature license smart

hostname leaf1
vdc leaf1 id 1
  limit-resource vlan minimum 16 maximum 4094
  limit-resource vrf minimum 2 maximum 4096
  limit-resource port-channel minimum 0 maximum 511
  limit-resource u4route-mem minimum 248 maximum 248
  limit-resource u6route-mem minimum 96 maximum 96
  limit-resource m4route-mem minimum 58 maximum 58
  limit-resource m6route-mem minimum 8 maximum 8

nv overlay evpn
feature ospf
feature bgp
feature interface-vlan
feature vn-segment-vlan-based
feature lldp
feature nv overlay

no password strength-check
username admin password 5 $5$IMPPOA$FKZOcnOt1vvWWOwbZ7AvpaddLpwD4FFMEB1TWSsFU78  role network-admin
ip domain-lookup
copp profile strict
snmp-server user admin network-admin auth md5 0x48546718e5073798226bc2991711f82e priv 0x48546718e5073798226bc2991711f82e localizedkey
rmon event 1 log trap public description FATAL(1) owner PMON@FATAL
rmon event 2 log trap public description CRITICAL(2) owner PMON@CRITICAL
rmon event 3 log trap public description ERROR(3) owner PMON@ERROR
rmon event 4 log trap public description WARNING(4) owner PMON@WARNING
rmon event 5 log trap public description INFORMATION(5) owner PMON@INFO
callhome
  email-contact sch-smart-licensing@cisco.com
  destination-profile CiscoTAC-1 transport-method http
  destination-profile CiscoTAC-1 index 1 http https://tools.cisco.com/its/service/oddce/services/DDCEService
  transport http use-vrf management
  enable

fabric forwarding anycast-gateway-mac 2020.0000.00aa
vlan 1,100,300
vlan 100
  vn-segment 10100
vlan 300
  vn-segment 10300

vrf context management
vrf context vxlan-10300
  vni 10300
  rd auto
  address-family ipv4 unicast
    route-target import 10300:100
    route-target import 10300:100 evpn
    route-target export 10300:100
    route-target export 10300:100 evpn

interface Vlan1

interface Vlan100
  no shutdown
  vrf member vxlan-10300
  ip address 192.168.100.254/24
  fabric forwarding mode anycast-gateway

interface Vlan300
  no shutdown
  vrf member vxlan-10300
  ip forward

interface nve1
  no shutdown
  host-reachability protocol bgp
  source-interface loopback1
  member vni 10100
    ingress-replication protocol bgp
  member vni 10300 associate-vrf

interface Ethernet1/1
  no switchport
  mtu 9216
  ip address 192.168.10.2/24
  ip ospf network point-to-point
  ip router ospf 100 area 0.0.0.0
  no shutdown

interface Ethernet1/2

interface Ethernet1/3

interface Ethernet1/4

interface Ethernet1/5
  switchport access vlan 100

interface Ethernet1/6

interface Ethernet1/7

interface Ethernet1/8

interface Ethernet1/9

interface Ethernet1/10

interface Ethernet1/11

interface Ethernet1/12

interface Ethernet1/13

interface Ethernet1/14

interface Ethernet1/15

interface Ethernet1/16

interface Ethernet1/17

interface Ethernet1/18

interface Ethernet1/19

interface Ethernet1/20

interface Ethernet1/21

interface Ethernet1/22

interface Ethernet1/23

interface Ethernet1/24

interface Ethernet1/25

interface Ethernet1/26

interface Ethernet1/27

interface Ethernet1/28

interface Ethernet1/29

interface Ethernet1/30

interface Ethernet1/31

interface Ethernet1/32

interface Ethernet1/33

interface Ethernet1/34

interface Ethernet1/35

interface Ethernet1/36

interface Ethernet1/37

interface Ethernet1/38

interface Ethernet1/39

interface Ethernet1/40

interface Ethernet1/41

interface Ethernet1/42

interface Ethernet1/43

interface Ethernet1/44

interface Ethernet1/45

interface Ethernet1/46

interface Ethernet1/47

interface Ethernet1/48

interface Ethernet1/49

interface Ethernet1/50

interface Ethernet1/51

interface Ethernet1/52

interface Ethernet1/53

interface Ethernet1/54

interface Ethernet1/55

interface Ethernet1/56

interface Ethernet1/57

interface Ethernet1/58

interface Ethernet1/59

interface Ethernet1/60

interface Ethernet1/61

interface Ethernet1/62

interface Ethernet1/63

interface Ethernet1/64

interface mgmt0
  vrf member management

interface loopback0
  ip address 1.1.1.1/32
  ip router ospf 100 area 0.0.0.0

interface loopback1
  ip address 11.11.11.11/32
  ip router ospf 100 area 0.0.0.0
icam monitor scale

line console
line vty
boot nxos bootflash:/nxos.9.3.7.bin sup-1
router ospf 100
  router-id 1.1.1.1
router bgp 10000
  neighbor 10.10.10.10
    remote-as 10000
    update-source loopback0
    address-family l2vpn evpn
      send-community
      send-community extended
  vrf vxlan-10300
    address-family ipv4 unicast
      network 192.168.100.0/24
evpn
  vni 10100 l2
    rd auto
    route-target import auto
    route-target export auto



leaf1# 